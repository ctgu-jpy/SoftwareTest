package org.example;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

import java.text.ParseException;

public class test01 {
    @Test
    @DisplayName("jiangpengyu")
    public void demoTest01(){
        System.out.println("hello,world");
    }

    @ParameterizedTest
    @CsvSource(value = {
            "20210410150000,20210490000000",
            "20210404191700,20210404191730",
            "20211031015959, 20211031020030",
            "20200505050505,20200505151515"
            })
    @DisplayName("test ")
    public void test_class_PhoneBill(String startTime, String endTime) throws ParseException {
        BillTest testClass = new BillTest();
        testClass.Bill(startTime, endTime);
    }
}
