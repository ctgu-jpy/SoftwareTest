package org.example;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvFileSource;
import org.junit.jupiter.params.provider.CsvSource;

import java.util.concurrent.TransferQueue;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class Test01 {



/*对输入值取值-1，1，100，49*/
    @ParameterizedTest
    @CsvSource(value = {
        "-1, 1, 100, 输入错误",
        "1 , 100 , 50 , 非三角形"
    })
    public void lab2_BoundaryValue(int a , int b , int c , String expected){
        Triangle triangle = new Triangle();
        assertEquals(expected, triangle.classify(a , b , c));
    }

    /*对输入值取值 -1 ，1，2， 49， 99，100，101*/
    @ParameterizedTest
    @CsvSource(value = {
            "-1 , 1 , 2 , 输入错误",
            "1 , 2 , 49 , 非三角形",
            "2 , 49 , 49 , 等腰三角形",
            "99, 99 , 99 ,等边三角形"
    })
    public void lab2_Robustness (int a , int b , int c , String expected){
        Triangle triangle = new Triangle();
        assertEquals(expected , triangle.classify(a , b , c));
    }
    /*1 2 50 99 100*/
    @ParameterizedTest
    @CsvSource(value = {
            "1 , 2 , 50 , 非三角形",
            "1 , 50 , 99 , 非三角形",
            "1 , 2 , 100 , 非三角形",
            "2 , 2, 2 , 等边三角形",
            "100 , 99 , 100 , 等腰三角形",
            "2 , 1 , 2 , 等腰三角形"
    })
    public void lab2_Worst_Case_Boundary_Value(int a , int b , int c , String expected){
        Triangle triangle = new Triangle();
        assertEquals(expected , triangle.classify(a , b , c));
    }
    /* 0 1 2 50 99 100 101*/
    @ParameterizedTest
    @CsvSource(value = {
            "0 , 1, 2 , 输入错误" ,
            "1 , 2 , 50 , 非三角形",
            "50 , 50 , 99 , 等腰三角形",
            "99 , 99 , 99 , 等边三角形",
            "99 , 100 , 101 , 输入错误",
            "1 , 2 , 50 , 非三角形"
    })
    public void lab2_Worst_Case_Robustness_Boundary_Value (int a , int b , int c , String expected ){
        Triangle triangle = new Triangle();;
        assertEquals(expected , triangle.classify( a , b , c));
    }

}
