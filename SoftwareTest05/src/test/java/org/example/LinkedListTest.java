package org.example;

import org.example.LinkedList;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class LinkedListTest {
    @Test
    @DisplayName("null情况")
    void test01(){
        LinkedList<String> list = new LinkedList();
        list.remove(null);
        assertEquals(0 , list.size());
    }

    @Test
    @DisplayName("非null情况")
    void test02(){
        LinkedList<String> list = new LinkedList<>();
        list.add("i");
        list.add("am");

        assertEquals(true,list.remove("i"));
    }

    @Test
    @DisplayName("找不到目标的情况")
    void test03(){
        LinkedList<String> list = new LinkedList<>();
        list.add("i");
        list.add("am");

        assertEquals(false , list.remove("k"));
    }
    @Test
    @DisplayName("list中有东西，但是remove null")
    void test04(){
        LinkedList<String> list = new LinkedList<>();
        list.add("i");
        list.add("am");
        list.add(null);
        assertEquals(true , list.remove(null));
    }
}
