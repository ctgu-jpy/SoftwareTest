
import org.junit.jupiter.api.Test;

import org.junit.jupiter.api.DisplayName;

import static junit.framework.TestCase.assertEquals;

/***
 *
 * @author tiger
 * @date 2021年3月4日-下午8:43:13
 * @description 三角形测试用例
 */
class TriangleTest {
    @Test
    @DisplayName("输入错误")
    public void  parameters_error_test() {
        Triangle triangle = new Triangle();
        String type = triangle.classify(0, 4, 5);
        assertEquals("输入错误", type);
    }
    @Test
    @DisplayName("不等边三角形")
    public void scalene_test() {
        Triangle triangle = new Triangle();
        String type = triangle.classify(3, 4, 6);
        assertEquals("不等边三角形", type);
    }
    @Test
    @DisplayName("等边三角形")
    public void equilateral_test(){
        Triangle triangle= new Triangle();
        String type = triangle.classify(3,3,3);
        assertEquals("等边三角形",type);
    }
    @Test
    @DisplayName("等边三角形")
    public void isosceles_test(){
        Triangle triangle= new Triangle();
        String type = triangle.classify(3,4,3);
        assertEquals("等腰三角形",type);
    }

    @Test
    @DisplayName("等边三角形")
    public void not_equilateral_test(){
        Triangle triangle= new Triangle();
        String type = triangle.classify(5,4,3);
        assertEquals("不等边三角形",type);
    }
}